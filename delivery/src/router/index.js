import Vue from 'vue'
import Router from 'vue-router'
import DeliveryShop from '../DeliveryShop'
import App from '../App'
import Main from '../Main'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/shop',
      name: 'Shop',
      component: DeliveryShop
    },

    {
      path: '/menu',
      name: 'Menu',
      component: App
    },

    {
      path: '/',
      redirect: {
        name: 'Menu'
      }
    }
  ]
})
