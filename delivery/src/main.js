import Vue from 'vue'
import App from './App.vue'
import Main from './Main.vue'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VModal from 'vue-js-modal'

Vue.use(VModal)


new Vue({
  el: '#app',
  router,
  render: h => h(Main)
})
